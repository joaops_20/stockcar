/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.DashboardEscritorioDto;
import br.com.stockcar.app.dto.EscritorioDto;
import br.com.stockcar.app.model.Escritorio;
import br.com.stockcar.app.model.Pagamento;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.EscritorioRepository;
import br.com.stockcar.app.repository.PagamentoRepository;
import br.com.stockcar.app.service.EscritorioService;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author João
 */
@Service("EscritorioService")
public class EscritorioServiceImpl implements EscritorioService {
    
    @Autowired
    private EscritorioRepository escritorioRepository;
    
    
    @Autowired
    private PagamentoRepository pagamentoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public EscritorioDto findOne(String codigoEscritorio) {
        Escritorio escritorio = escritorioRepository.findOne(codigoEscritorio);
        EscritorioDto escritorioDto = null;
        if (escritorio != null) {
            escritorioDto = new EscritorioDto();
            mapper.map(escritorio, escritorioDto);
        }
        return escritorioDto;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<EscritorioDto> findAll() {
        Iterable<Escritorio> list = escritorioRepository.findAll();
        List<EscritorioDto> listDto = new ArrayList<>();
        for (Escritorio escritorio : list) {
            EscritorioDto dto = new EscritorioDto();
            mapper.map(escritorio, dto);
            listDto.add(dto);
        }
        return listDto;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<EscritorioDto> searchAll(Pageable p) {
        Page<Escritorio> escritorios = escritorioRepository.findAll(p);
        List<EscritorioDto> listDto = new ArrayList<>();
        for (Escritorio escritorio : escritorios) {
            EscritorioDto dto = new EscritorioDto();
            mapper.map(escritorio, dto);
            listDto.add(dto);
        }
        Page<EscritorioDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, escritorios.getTotalElements());
        }
        return page;
    }
    
    @Override
    @Transactional(readOnly = true)
    public DashboardEscritorioDto findDashboard(Integer ano) {
        DashboardEscritorioDto dashboard = new DashboardEscritorioDto();
        dashboard.setAno(ano);
        dashboard.setJaneiro(getValorMes(ano, 1));
        dashboard.setFevereiro(getValorMes(ano, 2));
        dashboard.setMarco(getValorMes(ano, 3));
        dashboard.setAbril(getValorMes(ano, 4));
        dashboard.setMaio(getValorMes(ano, 5));
        dashboard.setJunho(getValorMes(ano, 6));
        dashboard.setJulho(getValorMes(ano, 7));
        dashboard.setAgosto(getValorMes(ano, 8));
        dashboard.setSetembro(getValorMes(ano, 9));
        dashboard.setOutubro(getValorMes(ano, 10));
        dashboard.setNovembro(getValorMes(ano, 11));
        dashboard.setDezembro(getValorMes(ano, 12));
        return dashboard;
    }
    
    @Transactional(readOnly = true)
    private Double getValorMes(Integer ano, Integer mes) {
        try {
            Date mesInicio, mesFim;
            Double valor = 0D;
            Calendar c = Calendar.getInstance();
            c.set(ano, mes, 1, 0, 0, 0);
            mesInicio = c.getTime();
            c.set(ano, mes, 31, 23, 59, 59);
            mesFim = c.getTime();
            List<Pagamento> pagamentos = pagamentoRepository.findByDataPagamentoBetween(mesInicio, mesFim);
            for (Pagamento pagamento : pagamentos) {
                valor += pagamento.getValor();
            }
            return valor;
        } catch (Exception e) {
            return 0D;
        }
    }
    
}