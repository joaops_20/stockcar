/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.ProdutoDto;
import br.com.stockcar.app.model.Produto;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.ProdudoRepository;
import br.com.stockcar.app.service.ProdutoService;

/**
 *
 * @author João
 */
@Service("ProdutoService")
public class ProdutoServiceImpl implements ProdutoService {
    
    @Autowired
    private ProdudoRepository produdoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public Page<ProdutoDto> searchAll(Pageable p) {
        Page<Produto> produtos = produdoRepository.findAll(p);
        List<ProdutoDto> listDto = new ArrayList<>();
        for (Produto produto : produtos) {
            ProdutoDto produtoDto = new ProdutoDto();
            produtoDto.setCodigoProduto(produto.getCodigoProduto());
            produtoDto.setNomeProduto(produto.getNomeProduto());
            produtoDto.setLinhaProduto(produto.getLinhaProduto());
            produtoDto.setEscalaProduto(produto.getEscalaProduto());
            produtoDto.setFornecedorProduto(produto.getFornecedorProduto());
            produtoDto.setDescricaoProduto(produto.getDescricaoProduto());
            produtoDto.setQuantidadeEstoque(produto.getQuantidadeEstoque());
            produtoDto.setPrecoCompra(produto.getPrecoCompra());
            produtoDto.setMsrp(produto.getMsrp());
            listDto.add(produtoDto);
        }
        Page<ProdutoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, produtos.getTotalElements());
        }
        return page;
    }
    
}