/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.SystemUserDto;
import br.com.stockcar.app.dto.SystemUserFormDto;
import br.com.stockcar.app.model.SystemUser;
import br.com.stockcar.app.repository.SystemUserRepository;
import br.com.stockcar.app.service.SystemUserService;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author João Paulo
 */
@Service("SystemUserService")
public class SystemUserServiceImpl implements SystemUserService {
    
    @Autowired
    private SystemUserRepository repository;
    
    @Autowired
    private PasswordEncoder encoder;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    public SystemUserFormDto newSystemUser() {
        return new SystemUserFormDto();
    }
    
    @Override
    public SystemUserDto save(SystemUserFormDto sysuser) {
        SystemUser user = new SystemUser();
        user.setId(sysuser.getId());
        user.setFirstName(sysuser.getFirstName());
        user.setMiddleName(sysuser.getMiddleName());
        user.setLastName(sysuser.getLastName());
        user.setEmail(sysuser.getEmail());
        user.setPassword(encoder.encode(sysuser.getPassword()));
        user.setAccountExpiration(sysuser.getAccountExpiration());
        user.setAccountCanExpire(sysuser.getAccountCanExpire());
        user.setLocked(sysuser.getLocked());
        user.setCredentialExpiration(sysuser.getCredentialExpiration());
        user.setCredentialCanExpire(sysuser.getCredentialCanExpire());
        user.setEnabled(sysuser.getEnabled());
        user = repository.save(user);
        SystemUserDto userDto = new SystemUserDto();
        mapper.map(user, userDto);
        return userDto;
    }

    @Override
    public SystemUserDto save(SystemUserDto userDto) {
        SystemUser user = new SystemUser();
        mapper.map(userDto, user);
        user = repository.save(user);
        SystemUserDto salvo = null;
        if (user != null) {
            salvo = new SystemUserDto();
            mapper.map(user, salvo);
        }
        return salvo;
    }
    
    @Transactional(readOnly = true)
    @Override
    public SystemUserDto getUserByEmail(String email) {
        SystemUser user = repository.findOneByEmail(email);
        SystemUserDto userDto = null;
        if (user != null) {
            userDto = new SystemUserDto();
            mapper.map(user, userDto);
        }
        return userDto;
    }
    
    @Transactional(readOnly = true)
    @Override
    public SystemUserDto findOne(Long id) {
        SystemUser user = repository.findOne(id);
        SystemUserDto userDto = null;
        if (user != null) {
            userDto = new SystemUserDto();
            mapper.map(user, userDto);
        }
        return userDto;
    }
    
    @Transactional(readOnly = true)
    @Override
    public SystemUserFormDto findOneSystemUserFormDto(Long id) {
        SystemUser user = repository.findOne(id);
        SystemUserFormDto userDto = null;
        if (user != null) {
            userDto = new SystemUserFormDto();
            userDto.setId(user.getId());
            userDto.setFirstName(user.getFirstName());
            userDto.setMiddleName(user.getMiddleName());
            userDto.setLastName(user.getLastName());
            userDto.setEmail(user.getEmail());
            userDto.setConfirmEmail(user.getEmail());
            userDto.setPassword("");//como decodificar a senha?
            userDto.setConfirmPassword("");
            userDto.setAccountExpiration(user.getAccountExpiration());
            userDto.setAccountCanExpire(user.getAccountCanExpire());
            userDto.setLocked(user.getLocked());
            userDto.setCredentialExpiration(user.getCredentialExpiration());
            userDto.setCredentialCanExpire(user.getCredentialCanExpire());
            userDto.setEnabled(user.getEnabled());
        }
        return userDto;
    }
    
    @Override
    public void delete(SystemUserDto userDto) {
        try {
            SystemUser user = new SystemUser();
            mapper.map(userDto, user);
            repository.delete(user);
        } catch (Exception e) {
        }
    }
    
    @Transactional(readOnly = true)
    @Override
    public Page<SystemUserDto> searchAllUsers(Pageable p) {
        List<SystemUserDto> usersDto = new ArrayList<>();
        Page<SystemUser> systemUsers = repository.findAll(p);
        for (SystemUser systemUser : systemUsers) {
            SystemUserDto userDto = new SystemUserDto();
            mapper.map(systemUser, userDto);
            usersDto.add(userDto);
        }
        Page<SystemUserDto> page = null;
        if (!usersDto.isEmpty()) {
            page = new PageImpl<>(usersDto, p, systemUsers.getTotalElements());
        }
        return page;
    }
    
}