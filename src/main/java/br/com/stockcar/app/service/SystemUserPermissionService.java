/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service;

import br.com.stockcar.app.dto.SystemUserPermissionDto;
import br.com.stockcar.app.dto.SystemUserPermissionFormDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author João Paulo
 */
public interface SystemUserPermissionService {
    
    public SystemUserPermissionFormDto newSystemUserPermission();
    public SystemUserPermissionDto save(SystemUserPermissionFormDto userPermissionFormDto);
    public void delete(Long idUser, Long idModule);
    public SystemUserPermissionDto findOne(Long idUser, Long idModule);
    public Page<SystemUserPermissionDto> searchAllUsersPermissions(Pageable p);
    
}