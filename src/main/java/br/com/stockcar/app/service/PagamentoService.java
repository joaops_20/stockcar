/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service;

import br.com.stockcar.app.dto.PagamentoDto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author João Paulo
 */
public interface PagamentoService {
    
    public List<Object[]> pegarSomaTotalValoresPelosAnos();
    public Page<PagamentoDto> searchAll(Pageable p);
    public Page<PagamentoDto> searchAllByCustomerNumber(Integer customerNumber, Pageable p);
    
}