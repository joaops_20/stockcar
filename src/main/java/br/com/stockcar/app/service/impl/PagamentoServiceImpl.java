/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.PagamentoDto;
import br.com.stockcar.app.model.Pagamento;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.PagamentoRepository;
import br.com.stockcar.app.service.PagamentoService;

/**
 *
 * @author João Paulo
 */
@Service("PagamentoService")
public class PagamentoServiceImpl implements PagamentoService {
    
    @Autowired
    private PagamentoRepository pagamentoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    public List<Object[]> pegarSomaTotalValoresPelosAnos() {
        Iterable<Pagamento> list = pagamentoRepository.findAll();
        List<Object[]> dados = new ArrayList<>();
        for (Pagamento pagamento : list) {
            Object[] d = new Object[2];
            d[0] = pagamento.getValor();
            d[1] = pagamento.getDataPagamento();
            dados.add(d);
        }
        return dados;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<PagamentoDto> searchAll(Pageable p) {
        Page<Pagamento> pagamentos = pagamentoRepository.findAll(p);
        List<PagamentoDto> listDto = new ArrayList<>();
        for (Pagamento pagamento : pagamentos) {
            PagamentoDto dto = new PagamentoDto();
            mapper.map(pagamento, dto);
            listDto.add(dto);
        }
        Page<PagamentoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, pagamentos.getTotalElements());
        }
        return page;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<PagamentoDto> searchAllByCustomerNumber(Integer customerNumber, Pageable p) {
        Page<Pagamento> pagamentos = pagamentoRepository.findAllByClienteNumeroCliente(customerNumber, p);
        List<PagamentoDto> listDto = new ArrayList<>();
        for (Pagamento pagamento : pagamentos) {
            PagamentoDto dto = new PagamentoDto();
            mapper.map(pagamento, dto);
            listDto.add(dto);
        }
        Page<PagamentoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, pagamentos.getTotalElements());
        }
        return page;
    }
    
}