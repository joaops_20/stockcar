/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service;

import br.com.stockcar.app.dto.EmpregadoDto;
import br.com.stockcar.app.dto.EscritorioDto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author João
 */
public interface EmpregadoService {
    
    public List<EmpregadoDto> findAllByEscritorio(EscritorioDto escritorioDto);
    public Page<EmpregadoDto> searchAll(Pageable p);
    
}