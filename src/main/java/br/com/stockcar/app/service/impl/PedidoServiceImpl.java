/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.ClienteDto;
import br.com.stockcar.app.dto.PedidoDto;
import br.com.stockcar.app.model.Cliente;
import br.com.stockcar.app.model.Pedido;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.PedidoRepository;
import br.com.stockcar.app.service.PedidoService;

/**
 *
 * @author João
 */
@Service("PedidoService")
public class PedidoServiceImpl implements PedidoService {
    
    @Autowired
    private PedidoRepository pedidoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public Page<PedidoDto> searchAll(Pageable p) {
        Page<Pedido> pedidos = pedidoRepository.findAll(p);
        List<PedidoDto> listDto = new ArrayList<>();
        for (Pedido pedido : pedidos) {
            PedidoDto pedidoDto = new PedidoDto();
            pedidoDto.setNumeroPedido(pedido.getNumeroPedido());
            pedidoDto.setDataPedido(pedido.getDataPedido());
            pedidoDto.setDataRequerida(pedido.getDataRequerida());
            pedidoDto.setDataEnvio(pedido.getDataEnvio());
            pedidoDto.setStatusPedido(pedido.getStatusPedido());
            pedidoDto.setComentario(pedido.getComentario());
            Cliente cliente = pedido.getCliente();
            ClienteDto clienteDto = new ClienteDto();
            mapper.map(cliente, clienteDto);
            pedidoDto.setCliente(clienteDto);
            listDto.add(pedidoDto);
        }
        Page<PedidoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, pedidos.getTotalElements());
        }
        return page;
    }
    
}