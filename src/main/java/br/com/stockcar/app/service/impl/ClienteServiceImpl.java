/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.ClienteDto;
import br.com.stockcar.app.model.Cliente;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.ClienteRepository;
import br.com.stockcar.app.service.ClienteService;

/**
 *
 * @author João
 */
@Service("ClienteService")
public class ClienteServiceImpl implements ClienteService {
    
    @Autowired
    private ClienteRepository clienteRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public Page<ClienteDto> searchAll(Pageable p) {
        Page<Cliente> clientes = clienteRepository.findAll(p);
        List<ClienteDto> listDto = new ArrayList<>();
        for (Cliente cliente : clientes) {
            ClienteDto dto = new ClienteDto();
            mapper.map(cliente, dto);
            listDto.add(dto);
        }
        Page<ClienteDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, clientes.getTotalElements());
        }
        return page;
    }
    
}