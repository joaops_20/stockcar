/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.EmpregadoDto;
import br.com.stockcar.app.dto.EscritorioDto;
import br.com.stockcar.app.model.Empregado;
import br.com.stockcar.app.model.Escritorio;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import br.com.stockcar.app.repository.EmpregadoRepository;
import br.com.stockcar.app.service.EmpregadoService;

/**
 *
 * @author João
 */
@Service("EmpregadoService")
public class EmpregadoServiceImpl implements EmpregadoService {
    
    @Autowired
    private EmpregadoRepository empregadoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public List<EmpregadoDto> findAllByEscritorio(EscritorioDto escritorioDto) {
        Escritorio escritorio = new Escritorio();
        mapper.map(escritorioDto, escritorio);
        List<Empregado> list = empregadoRepository.findByEscritorio(escritorio);
        List<EmpregadoDto> listDto = new ArrayList<>();
        EmpregadoDto dto;
        for (Empregado empregado : list) {
            dto = new EmpregadoDto();
            mapper.map(empregado, dto);
            listDto.add(dto);
        }
        return listDto;
    }
    
    @Override
    @Transactional(readOnly = true)
    public Page<EmpregadoDto> searchAll(Pageable p) {
        Page<Empregado> empregados = empregadoRepository.findAll(p);
        List<EmpregadoDto> listDto = new ArrayList<>();
        for (Empregado empregado : empregados) {
            EmpregadoDto dto = new EmpregadoDto();
            mapper.map(empregado, dto);
            listDto.add(dto);
        }
        Page<EmpregadoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, empregados.getTotalElements());
        }
        return page;
    }
    
}