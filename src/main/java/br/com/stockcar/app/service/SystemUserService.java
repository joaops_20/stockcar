/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service;

import br.com.stockcar.app.dto.SystemUserDto;
import br.com.stockcar.app.dto.SystemUserFormDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author João Paulo
 */
public interface SystemUserService {
    
    public SystemUserFormDto newSystemUser();
    public SystemUserDto save(SystemUserFormDto sysuser);
    public SystemUserDto save(SystemUserDto userDto);
    public SystemUserDto getUserByEmail(String email);
    public SystemUserDto findOne(Long id);
    public SystemUserFormDto findOneSystemUserFormDto(Long id);
    public void delete(SystemUserDto userDto);
    public Page<SystemUserDto> searchAllUsers(Pageable p);
    
}