/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service;

import br.com.stockcar.app.dto.DashboardEscritorioDto;
import br.com.stockcar.app.dto.EscritorioDto;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author João
 */
public interface EscritorioService {
    
    public EscritorioDto findOne(String codigoEscritorio);
    public List<EscritorioDto> findAll();
    public Page<EscritorioDto> searchAll(Pageable p);
    public DashboardEscritorioDto findDashboard(Integer ano);
    
}