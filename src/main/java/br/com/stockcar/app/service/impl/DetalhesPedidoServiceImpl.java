/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.service.impl;

import br.com.stockcar.app.dto.ClienteDto;
import br.com.stockcar.app.dto.DetalhesPedidoDto;
import br.com.stockcar.app.dto.DetalhesPedidoIdDto;
import br.com.stockcar.app.dto.PedidoDto;
import br.com.stockcar.app.dto.ProdutoDto;
import br.com.stockcar.app.model.Cliente;
import br.com.stockcar.app.model.DetalhesPedido;
import br.com.stockcar.app.model.DetalhesPedidoId;
import br.com.stockcar.app.model.Pedido;
import br.com.stockcar.app.model.Produto;
import java.util.ArrayList;
import java.util.List;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import br.com.stockcar.app.repository.DetalhesPedidoRepository;
import br.com.stockcar.app.service.DetalhesPedidoService;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author João Paulo
 */
@Service("DetalhesPedidoService")
public class DetalhesPedidoServiceImpl implements DetalhesPedidoService {
    
    @Autowired
    private DetalhesPedidoRepository detalhesPedidoRepository;
    
    @Autowired
    private Mapper mapper;
    
    @Override
    @Transactional(readOnly = true)
    public Page<DetalhesPedidoDto> searchAll(Pageable p) {
        Page<DetalhesPedido> detalhesPedido = detalhesPedidoRepository.findAll(p);
        List<DetalhesPedidoDto> listDto = new ArrayList<>();
        for (DetalhesPedido detalhePedido : detalhesPedido) {
            DetalhesPedidoDto dto = new DetalhesPedidoDto();
            DetalhesPedidoIdDto idDto = new DetalhesPedidoIdDto();
            Pedido pedido = detalhePedido.getDetalhesPedidoId().getPedido();
            PedidoDto pedidoDto = new PedidoDto();
            pedidoDto.setNumeroPedido(pedido.getNumeroPedido());
            pedidoDto.setDataPedido(pedido.getDataPedido());
            pedidoDto.setDataRequerida(pedido.getDataRequerida());
            pedidoDto.setDataEnvio(pedido.getDataEnvio());
            pedidoDto.setStatusPedido(pedido.getStatusPedido());
            pedidoDto.setComentario(pedido.getComentario());
            Cliente cliente = pedido.getCliente();
            ClienteDto clienteDto = new ClienteDto();
            mapper.map(cliente, clienteDto);
            pedidoDto.setCliente(clienteDto);
            idDto.setPedido(pedidoDto);
            Produto produto = detalhePedido.getDetalhesPedidoId().getProduto();
            ProdutoDto produtoDto = new ProdutoDto();
            produtoDto.setCodigoProduto(produto.getCodigoProduto());
            produtoDto.setNomeProduto(produto.getNomeProduto());
            produtoDto.setLinhaProduto(produto.getLinhaProduto());
            produtoDto.setEscalaProduto(produto.getEscalaProduto());
            produtoDto.setFornecedorProduto(produto.getFornecedorProduto());
            produtoDto.setDescricaoProduto(produto.getDescricaoProduto());
            produtoDto.setQuantidadeEstoque(produto.getQuantidadeEstoque());
            produtoDto.setPrecoCompra(produto.getPrecoCompra());
            produtoDto.setMsrp(produto.getMsrp());
            idDto.setProduto(produtoDto);
            dto.setDetalhesPedidoId(idDto);
            dto.setQuantidadeEncomendada(detalhePedido.getQuantidadeEncomendada());
            dto.setPrecoUnidade(detalhePedido.getPrecoUnidade());
            dto.setNumeroLinhaPedido(detalhePedido.getNumeroLinhaPedido());
            listDto.add(dto);
        }
        Page<DetalhesPedidoDto> page = null;
        if (!listDto.isEmpty()) {
            page = new PageImpl<>(listDto, p, detalhesPedido.getTotalElements());
        }
        return page;
    }
    
}