/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.configuration;

import br.com.stockcar.app.security.AuthenticationProviderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

/**
 *
 * @author João Paulo
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    private final String defaultSecret = "ee3e40f590e8f63da4e811a462db4518245f34fb2a42278ed730b1ad19fc3cd21eaaf85b63156d62706860417c4180503cfa1501e12aad3381199d2faaacfcbd"; //Olá Mundo!!! em Criptografia Hash SHA-512
    
    @Autowired
    private AuthenticationProviderImpl authenticatorProvider;
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.eraseCredentials(true);
        auth.authenticationProvider(authenticatorProvider);
    }
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/resources/**");
    }
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").authenticated()
                .antMatchers("/system/user").hasRole("SYSTEM_USER_READ")
                .antMatchers("/system/user/add").hasRole("SYSTEM_USER_ADD")
                .antMatchers("/system/user/save").hasRole("SYSTEM_USER_ADD")
                .antMatchers("/system/user/edit/*").hasRole("SYSTEM_USER_EDIT")
                .antMatchers("/system/user/update").hasRole("SYSTEM_USER_EDIT")
                .antMatchers("/system/user/delete/*").hasRole("SYSTEM_USER_DELETE")
                .antMatchers("/system/module").hasRole("SYSTEM_MODULE_READ")
                .antMatchers("/system/module/add").hasRole("SYSTEM_MODULE_ADD")
                .antMatchers("/system/module/save").hasRole("SYSTEM_MODULE_ADD")
                .antMatchers("/system/module/edit/*").hasRole("SYSTEM_MODULE_EDIT")
                .antMatchers("/system/module/update").hasRole("SYSTEM_MODULE_EDIT")
                .antMatchers("/system/module/delete/*").hasRole("SYSTEM_MODULE_DELETE")
                .antMatchers("/system/permission").hasRole("SYSTEM_PERMISSION_READ")
                .antMatchers("/system/permission/add").hasRole("SYSTEM_PERMISSION_ADD")
                .antMatchers("/system/permission/save").hasRole("SYSTEM_PERMISSION_ADD")
                .antMatchers("/system/permission/edit/*").hasRole("SYSTEM_PERMISSION_EDIT")
                .antMatchers("/system/permission/update").hasRole("SYSTEM_PERMISSION_EDIT")
                .antMatchers("/system/permission/delete/*").hasRole("SYSTEM_PERMISSION_DELETE")
                .antMatchers("/logout").permitAll()
                .anyRequest().authenticated();
        
        http.formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/login/check")
                .failureUrl("/login/error")
                .defaultSuccessUrl("/", true)
                .usernameParameter("username")
                .passwordParameter("password")
                .permitAll();
        
        http.logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/logout")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");
        
        http.sessionManagement()
                .maximumSessions(1).and()
                .sessionFixation()
                .newSession();
    }
    
    @Bean
    public PasswordEncoder passwordEncoder() {
        StandardPasswordEncoder encoder = new StandardPasswordEncoder(defaultSecret);
        return encoder;
    }
    
}