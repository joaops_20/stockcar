/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.repository;

import br.com.stockcar.app.model.Pagamento;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author João Paulo
 */
public interface PagamentoRepository extends PagingAndSortingRepository<Pagamento, String> {
    
    List<Pagamento> findByDataPagamentoBetween(Date start, Date end);
    public Page<Pagamento> findAllByClienteNumeroCliente(Integer numeroCliente, Pageable p);
    
}