/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "pedido", 
        indexes = {
            @Index(name = "idx_numero_pedido", columnList = "numero_pedido")
        }
)
public class Pedido implements Serializable {
    
    @Id
    @Column(name = "numero_pedido", length = 11)
    private Integer numeroPedido;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_pedido", nullable = false)
    private Date dataPedido;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_requerida", nullable = false)
    private Date dataRequerida;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_envio", nullable = true)
    private Date dataEnvio;
    
    @Column(name = "status_pedido", nullable = false, length = 15)
    private String statusPedido;
    
    @Column(name = "comentario", nullable = true, length = 1000)
    private String comentario;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "numero_cliente", nullable = false)
    private Cliente cliente;
    
    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "detalhesPedidoId.pedido", fetch = FetchType.LAZY)
    private List<DetalhesPedido> detalhesPedido;
    
    public Integer getNumeroPedido() {
        return numeroPedido;
    }
    
    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }
    
    public Date getDataPedido() {
        return dataPedido;
    }
    
    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }
    
    public Date getDataRequerida() {
        return dataRequerida;
    }
    
    public void setDataRequerida(Date dataRequerida) {
        this.dataRequerida = dataRequerida;
    }
    
    public Date getDataEnvio() {
        return dataEnvio;
    }
    
    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }
    
    public String getStatusPedido() {
        return statusPedido;
    }
    
    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }
    
    public String getComentario() {
        return comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    public Cliente getCliente() {
        return cliente;
    }
    
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    public List<DetalhesPedido> getDetalhesPedido() {
        return detalhesPedido;
    }
    
    public void setDetalhesPedido(List<DetalhesPedido> detalhesPedido) {
        this.detalhesPedido = detalhesPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.numeroPedido);
        hash = 97 * hash + Objects.hashCode(this.dataPedido);
        hash = 97 * hash + Objects.hashCode(this.dataRequerida);
        hash = 97 * hash + Objects.hashCode(this.dataEnvio);
        hash = 97 * hash + Objects.hashCode(this.statusPedido);
        hash = 97 * hash + Objects.hashCode(this.comentario);
        hash = 97 * hash + Objects.hashCode(this.cliente);
        hash = 97 * hash + Objects.hashCode(this.detalhesPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (!Objects.equals(this.statusPedido, other.statusPedido)) {
            return false;
        }
        if (!Objects.equals(this.comentario, other.comentario)) {
            return false;
        }
        if (!Objects.equals(this.numeroPedido, other.numeroPedido)) {
            return false;
        }
        if (!Objects.equals(this.dataPedido, other.dataPedido)) {
            return false;
        }
        if (!Objects.equals(this.dataRequerida, other.dataRequerida)) {
            return false;
        }
        if (!Objects.equals(this.dataEnvio, other.dataEnvio)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.detalhesPedido, other.detalhesPedido)) {
            return false;
        }
        return true;
    }
    
}