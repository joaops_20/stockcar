/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "escritorio",
        indexes = {
            @Index(name = "idx_codigo_escritorio", columnList = "codigo_escritorio")
        }
)
public class Escritorio implements Serializable {
    
    @Id
    @Column(name = "codigo_escritorio", length = 10)
    private String codigoEscritorio;
    
    @Column(name = "cidade", nullable = false, length = 50)
    private String cidade;
    
    @Column(name = "telefone", nullable = false, length = 50)
    private String telefone;
    
    @Column(name = "endereco1", nullable = false, length = 50)
    private String endereco1;
    
    @Column(name = "endereco2", nullable = true, length = 50)
    private String endereco2;
    
    @Column(name = "estado", nullable = true, length = 50)
    private String estado;
    
    @Column(name = "pais", nullable = false, length = 50)
    private String pais;
    
    @Column(name = "codigo_postal", nullable = false, length = 15)
    private String codigoPostal;
    
    @Column(name = "territorio", nullable = false, length = 10)
    private String territorio;
    
    public String getCodigoEscritorio() {
        return codigoEscritorio;
    }
    
    public void setCodigoEscritorio(String codigoEscritorio) {
        this.codigoEscritorio = codigoEscritorio;
    }
    
    public String getCidade() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getTelefone() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getEndereco1() {
        return endereco1;
    }
    
    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }
    
    public String getEndereco2() {
        return endereco2;
    }
    
    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public String getCodigoPostal() {
        return codigoPostal;
    }
    
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    public String getTerritorio() {
        return territorio;
    }
    
    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.codigoEscritorio);
        hash = 83 * hash + Objects.hashCode(this.cidade);
        hash = 83 * hash + Objects.hashCode(this.telefone);
        hash = 83 * hash + Objects.hashCode(this.endereco1);
        hash = 83 * hash + Objects.hashCode(this.endereco2);
        hash = 83 * hash + Objects.hashCode(this.estado);
        hash = 83 * hash + Objects.hashCode(this.pais);
        hash = 83 * hash + Objects.hashCode(this.codigoPostal);
        hash = 83 * hash + Objects.hashCode(this.territorio);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Escritorio other = (Escritorio) obj;
        if (!Objects.equals(this.codigoEscritorio, other.codigoEscritorio)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        if (!Objects.equals(this.telefone, other.telefone)) {
            return false;
        }
        if (!Objects.equals(this.endereco1, other.endereco1)) {
            return false;
        }
        if (!Objects.equals(this.endereco2, other.endereco2)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.codigoPostal, other.codigoPostal)) {
            return false;
        }
        if (!Objects.equals(this.territorio, other.territorio)) {
            return false;
        }
        return true;
    }
    
}