/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "cliente", 
        indexes = {
            @Index(name = "idx_numero_cliente", columnList = "numero_cliente")
        }
)
public class Cliente implements Serializable {
    
    @Id
    @Column(name = "numero_cliente", length = 11)
    private Integer numeroCliente;
    
    @Column(name = "nome_cliente", nullable = false, length = 50)
    private String nomeCliente;
    
    @Column(name = "ultimo_nome_contato", nullable = false, length = 50)
    private String ultimoNomeContato;
    
    @Column(name = "primeiro_nome_contato", nullable = false, length = 50)
    private String primeiroNomeContato;
    
    @Column(name = "telefone", nullable = false, length = 50)
    private String telefone;
    
    @Column(name = "endereco1", nullable = false, length = 50)
    private String endereco1;
    
    @Column(name = "endereco2", nullable = true, length = 50)
    private String endereco2;
    
    @Column(name = "cidade", nullable = false, length = 50)
    private String cidade;
    
    @Column(name = "estado", nullable = true, length = 50)
    private String estado;
    
    @Column(name = "codigo_postal", nullable = true, length = 15)
    private String codigoPostal;
    
    @Column(name = "pais", nullable = false, length = 50)
    private String pais;
    
    @Column(name = "limite_credito", nullable = true)
    private Double limiteCredito;
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name="vendedor", nullable = true)
    private Empregado empregado;
    
    public Integer getNumeroCliente() {
        return numeroCliente;
    }
    
    public void setNumeroCliente(Integer numeroCliente) {
        this.numeroCliente = numeroCliente;
    }
    
    public String getNomeCliente() {
        return nomeCliente;
    }
    
    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    
    public String getUltimoNomeContato() {
        return ultimoNomeContato;
    }
    
    public void setUltimoNomeContato(String ultimoNomeContato) {
        this.ultimoNomeContato = ultimoNomeContato;
    }
    
    public String getPrimeiroNomeContato() {
        return primeiroNomeContato;
    }
    
    public void setPrimeiroNomeContato(String primeiroNomeContato) {
        this.primeiroNomeContato = primeiroNomeContato;
    }
    
    public String getTelefone() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getEndereco1() {
        return endereco1;
    }
    
    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }
    
    public String getEndereco2() {
        return endereco2;
    }
    
    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }
    
    public String getCidade() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String getCodigoPostal() {
        return codigoPostal;
    }
    
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public Double getLimiteCredito() {
        return limiteCredito;
    }
    
    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }
    
    public Empregado getEmpregado() {
        return empregado;
    }
    
    public void setEmpregado(Empregado empregado) {
        this.empregado = empregado;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(this.numeroCliente);
        hash = 37 * hash + Objects.hashCode(this.nomeCliente);
        hash = 37 * hash + Objects.hashCode(this.ultimoNomeContato);
        hash = 37 * hash + Objects.hashCode(this.primeiroNomeContato);
        hash = 37 * hash + Objects.hashCode(this.telefone);
        hash = 37 * hash + Objects.hashCode(this.endereco1);
        hash = 37 * hash + Objects.hashCode(this.endereco2);
        hash = 37 * hash + Objects.hashCode(this.cidade);
        hash = 37 * hash + Objects.hashCode(this.estado);
        hash = 37 * hash + Objects.hashCode(this.codigoPostal);
        hash = 37 * hash + Objects.hashCode(this.pais);
        hash = 37 * hash + Objects.hashCode(this.limiteCredito);
        hash = 37 * hash + Objects.hashCode(this.empregado);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.nomeCliente, other.nomeCliente)) {
            return false;
        }
        if (!Objects.equals(this.ultimoNomeContato, other.ultimoNomeContato)) {
            return false;
        }
        if (!Objects.equals(this.primeiroNomeContato, other.primeiroNomeContato)) {
            return false;
        }
        if (!Objects.equals(this.telefone, other.telefone)) {
            return false;
        }
        if (!Objects.equals(this.endereco1, other.endereco1)) {
            return false;
        }
        if (!Objects.equals(this.endereco2, other.endereco2)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.codigoPostal, other.codigoPostal)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.numeroCliente, other.numeroCliente)) {
            return false;
        }
        if (!Objects.equals(this.limiteCredito, other.limiteCredito)) {
            return false;
        }
        if (!Objects.equals(this.empregado, other.empregado)) {
            return false;
        }
        return true;
    }
    
}