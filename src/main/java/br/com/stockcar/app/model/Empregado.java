/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "empregado", 
        indexes = {
            @Index(name = "idx_numero_empregado", columnList = "numero_empregado")
        }
)
public class Empregado implements Serializable {
    
    @Id
    @Column(name = "numero_empregado", length = 11)
    private Integer numeroEmpregado;
    
    @Column(name = "ultimo_nome", nullable = false, length = 50)
    private String ultimoNome;
    
    @Column(name = "primeiro_nome", nullable = false, length = 50)
    private String primeiroNome;
    
    @Column(name = "extensao", nullable = false, length = 10)
    private String extensao;
    
    @Column(name = "email", nullable = false, length = 100)
    private String email;
    
    @Column(name = "reporta_para", nullable = true, length = 11)
    private Integer reportaPara;
    
    @Column(name = "titulo_trabalho", nullable = false, length = 50)
    private String tituloTrabalho;
    
    @ManyToOne(cascade = CascadeType.ALL , fetch = FetchType.LAZY)
    @JoinColumn(name="codigo_escritorio", nullable = false)
    private Escritorio escritorio;
    
    public Integer getNumeroEmpregado() {
        return numeroEmpregado;
    }
    
    public void setNumeroEmpregado(Integer numeroEmpregado) {
        this.numeroEmpregado = numeroEmpregado;
    }
    
    public String getUltimoNome() {
        return ultimoNome;
    }
    
    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }
    
    public String getPrimeiroNome() {
        return primeiroNome;
    }
    
    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }
    
    public String getExtensao() {
        return extensao;
    }
    
    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public Integer getReportaPara() {
        return reportaPara;
    }
    
    public void setReportaPara(Integer reportaPara) {
        this.reportaPara = reportaPara;
    }
    
    public String getTituloTrabalho() {
        return tituloTrabalho;
    }
    
    public void setTituloTrabalho(String tituloTrabalho) {
        this.tituloTrabalho = tituloTrabalho;
    }
    
    public Escritorio getEscritorio() {
        return escritorio;
    }
    
    public void setEscritorio(Escritorio escritorio) {
        this.escritorio = escritorio;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.numeroEmpregado);
        hash = 47 * hash + Objects.hashCode(this.ultimoNome);
        hash = 47 * hash + Objects.hashCode(this.primeiroNome);
        hash = 47 * hash + Objects.hashCode(this.extensao);
        hash = 47 * hash + Objects.hashCode(this.email);
        hash = 47 * hash + Objects.hashCode(this.reportaPara);
        hash = 47 * hash + Objects.hashCode(this.tituloTrabalho);
        hash = 47 * hash + Objects.hashCode(this.escritorio);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empregado other = (Empregado) obj;
        if (!Objects.equals(this.ultimoNome, other.ultimoNome)) {
            return false;
        }
        if (!Objects.equals(this.primeiroNome, other.primeiroNome)) {
            return false;
        }
        if (!Objects.equals(this.extensao, other.extensao)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.tituloTrabalho, other.tituloTrabalho)) {
            return false;
        }
        if (!Objects.equals(this.numeroEmpregado, other.numeroEmpregado)) {
            return false;
        }
        if (!Objects.equals(this.reportaPara, other.reportaPara)) {
            return false;
        }
        if (!Objects.equals(this.escritorio, other.escritorio)) {
            return false;
        }
        return true;
    }
    
}