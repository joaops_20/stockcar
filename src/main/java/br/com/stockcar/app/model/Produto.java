/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "produto", 
        indexes = {
            @Index(name = "idx_codigo_produto", columnList = "codigo_produto")
        }
)
public class Produto implements Serializable {
    
    @Id
    @Column(name = "codigo_produto", length = 50)
    private String codigoProduto;
    
    @Column(name = "nome_produto", nullable = false, length = 70)
    private String nomeProduto;
    
    @Column(name = "linha_produto", nullable = false, length = 50)
    private String linhaProduto;
    
    @Column(name = "escala_produto", nullable = false, length = 10)
    private String escalaProduto;
    
    @Column(name = "fornecedor_produto", nullable = false, length = 50)
    private String fornecedorProduto;
    
    @Column(name = "descricao_produto", nullable = false, length = 1000)
    private String descricaoProduto;
    
    @Column(name = "quantidade_estoque", nullable = false, length = 6)
    private Integer quantidadeEstoque;
    
    @Column(name = "preco_compra", nullable = false)
    private Double precoCompra;
    
    @Column(name = "msrp", nullable = false)
    private Double msrp;
    
    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true, mappedBy = "detalhesPedidoId.produto", fetch = FetchType.LAZY)
    private List<DetalhesPedido> detalhesPedido;
    
    public String getCodigoProduto() {
        return codigoProduto;
    }
    
    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }
    
    public String getNomeProduto() {
        return nomeProduto;
    }
    
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }
    
    public String getLinhaProduto() {
        return linhaProduto;
    }
    
    public void setLinhaProduto(String linhaProduto) {
        this.linhaProduto = linhaProduto;
    }
    
    public String getEscalaProduto() {
        return escalaProduto;
    }
    
    public void setEscalaProduto(String escalaProduto) {
        this.escalaProduto = escalaProduto;
    }
    
    public String getFornecedorProduto() {
        return fornecedorProduto;
    }
    
    public void setFornecedorProduto(String fornecedorProduto) {
        this.fornecedorProduto = fornecedorProduto;
    }
    
    public String getDescricaoProduto() {
        return descricaoProduto;
    }
    
    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }
    
    public Integer getQuantidadeEstoque() {
        return quantidadeEstoque;
    }
    
    public void setQuantidadeEstoque(Integer quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public Double getPrecoCompra() {
        return precoCompra;
    }
    
    public void setPrecoCompra(Double precoCompra) {
        this.precoCompra = precoCompra;
    }
    
    public Double getMsrp() {
        return msrp;
    }
    
    public void setMsrp(Double msrp) {
        this.msrp = msrp;
    }
    
    public List<DetalhesPedido> getDetalhesPedido() {
        return detalhesPedido;
    }
    
    public void setDetalhesPedido(List<DetalhesPedido> detalhesPedido) {
        this.detalhesPedido = detalhesPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.codigoProduto);
        hash = 31 * hash + Objects.hashCode(this.nomeProduto);
        hash = 31 * hash + Objects.hashCode(this.linhaProduto);
        hash = 31 * hash + Objects.hashCode(this.escalaProduto);
        hash = 31 * hash + Objects.hashCode(this.fornecedorProduto);
        hash = 31 * hash + Objects.hashCode(this.descricaoProduto);
        hash = 31 * hash + Objects.hashCode(this.quantidadeEstoque);
        hash = 31 * hash + Objects.hashCode(this.precoCompra);
        hash = 31 * hash + Objects.hashCode(this.msrp);
        hash = 31 * hash + Objects.hashCode(this.detalhesPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Produto other = (Produto) obj;
        if (!Objects.equals(this.codigoProduto, other.codigoProduto)) {
            return false;
        }
        if (!Objects.equals(this.nomeProduto, other.nomeProduto)) {
            return false;
        }
        if (!Objects.equals(this.linhaProduto, other.linhaProduto)) {
            return false;
        }
        if (!Objects.equals(this.escalaProduto, other.escalaProduto)) {
            return false;
        }
        if (!Objects.equals(this.fornecedorProduto, other.fornecedorProduto)) {
            return false;
        }
        if (!Objects.equals(this.descricaoProduto, other.descricaoProduto)) {
            return false;
        }
        if (!Objects.equals(this.quantidadeEstoque, other.quantidadeEstoque)) {
            return false;
        }
        if (!Objects.equals(this.precoCompra, other.precoCompra)) {
            return false;
        }
        if (!Objects.equals(this.msrp, other.msrp)) {
            return false;
        }
        if (!Objects.equals(this.detalhesPedido, other.detalhesPedido)) {
            return false;
        }
        return true;
    }
    
}