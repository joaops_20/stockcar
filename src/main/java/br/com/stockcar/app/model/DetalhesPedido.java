/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.AssociationOverride;
import javax.persistence.AssociationOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

/**
 *
 * @author João Paulo
 */
@Entity
@Table(schema = "public", name = "detalhes_pedido")
@AssociationOverrides({
    @AssociationOverride(name = "detalhesPedidoId.pedido", joinColumns = @JoinColumn(name = "numero_pedido")),
    @AssociationOverride(name = "detalhesPedidoId.produto", joinColumns = @JoinColumn(name = "codigo_produto"))
})
public class DetalhesPedido implements Serializable {
    
    @EmbeddedId
    private DetalhesPedidoId detalhesPedidoId;
    
    @Column(name = "quantidade_encomendada", nullable = false, length = 11)
    private Integer quantidadeEncomendada;
    
    @Column(name = "preco_unidade", nullable = false)
    private Double precoUnidade;
    
    @Column(name = "numero_linha_pedido", nullable = false, length = 6)
    private Integer numeroLinhaPedido;
    
    public DetalhesPedidoId getDetalhesPedidoId() {
        return detalhesPedidoId;
    }
    
    public void setDetalhesPedidoId(DetalhesPedidoId detalhesPedidoId) {
        this.detalhesPedidoId = detalhesPedidoId;
    }
    
    public Integer getQuantidadeEncomendada() {
        return quantidadeEncomendada;
    }
    
    public void setQuantidadeEncomendada(Integer quantidadeEncomendada) {
        this.quantidadeEncomendada = quantidadeEncomendada;
    }
    
    public Double getPrecoUnidade() {
        return precoUnidade;
    }
    
    public void setPrecoUnidade(Double precoUnidade) {
        this.precoUnidade = precoUnidade;
    }
    
    public Integer getNumeroLinhaPedido() {
        return numeroLinhaPedido;
    }
    
    public void setNumeroLinhaPedido(Integer numeroLinhaPedido) {
        this.numeroLinhaPedido = numeroLinhaPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.detalhesPedidoId);
        hash = 67 * hash + Objects.hashCode(this.quantidadeEncomendada);
        hash = 67 * hash + Objects.hashCode(this.precoUnidade);
        hash = 67 * hash + Objects.hashCode(this.numeroLinhaPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalhesPedido other = (DetalhesPedido) obj;
        if (!Objects.equals(this.detalhesPedidoId, other.detalhesPedidoId)) {
            return false;
        }
        if (!Objects.equals(this.quantidadeEncomendada, other.quantidadeEncomendada)) {
            return false;
        }
        if (!Objects.equals(this.precoUnidade, other.precoUnidade)) {
            return false;
        }
        if (!Objects.equals(this.numeroLinhaPedido, other.numeroLinhaPedido)) {
            return false;
        }
        return true;
    }
    
}