/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class EscritorioDto implements Serializable {
    
    private String codigoEscritorio;
    private String cidade;
    private String telefone;
    private String endereco1;
    private String endereco2;
    private String estado;
    private String pais;
    private String codigoPostal;
    private String territorio;
    
    public String getCodigoEscritorio() {
        return codigoEscritorio;
    }
    
    public void setCodigoEscritorio(String codigoEscritorio) {
        this.codigoEscritorio = codigoEscritorio;
    }
    
    public String getCidade() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getTelefone() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getEndereco1() {
        return endereco1;
    }
    
    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }
    
    public String getEndereco2() {
        return endereco2;
    }
    
    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public String getCodigoPostal() {
        return codigoPostal;
    }
    
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    public String getTerritorio() {
        return territorio;
    }
    
    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.codigoEscritorio);
        hash = 41 * hash + Objects.hashCode(this.cidade);
        hash = 41 * hash + Objects.hashCode(this.telefone);
        hash = 41 * hash + Objects.hashCode(this.endereco1);
        hash = 41 * hash + Objects.hashCode(this.endereco2);
        hash = 41 * hash + Objects.hashCode(this.estado);
        hash = 41 * hash + Objects.hashCode(this.pais);
        hash = 41 * hash + Objects.hashCode(this.codigoPostal);
        hash = 41 * hash + Objects.hashCode(this.territorio);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EscritorioDto other = (EscritorioDto) obj;
        if (!Objects.equals(this.codigoEscritorio, other.codigoEscritorio)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        if (!Objects.equals(this.telefone, other.telefone)) {
            return false;
        }
        if (!Objects.equals(this.endereco1, other.endereco1)) {
            return false;
        }
        if (!Objects.equals(this.endereco2, other.endereco2)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.codigoPostal, other.codigoPostal)) {
            return false;
        }
        if (!Objects.equals(this.territorio, other.territorio)) {
            return false;
        }
        return true;
    }
    
}