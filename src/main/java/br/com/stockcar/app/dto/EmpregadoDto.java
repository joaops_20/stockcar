/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class EmpregadoDto implements Serializable {
    
    private Integer numeroEmpregado;
    private String ultimoNome;
    private String primeiroNome;
    private String extensao;
    private String email;
    private Integer reportaPara;
    private String tituloTrabalho;
    private EscritorioDto escritorio;
    
    public Integer getNumeroEmpregado() {
        return numeroEmpregado;
    }
    
    public void setNumeroEmpregado(Integer numeroEmpregado) {
        this.numeroEmpregado = numeroEmpregado;
    }
    
    public String getUltimoNome() {
        return ultimoNome;
    }
    
    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }
    
    public String getPrimeiroNome() {
        return primeiroNome;
    }
    
    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }
    
    public String getExtensao() {
        return extensao;
    }
    
    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public Integer getReportaPara() {
        return reportaPara;
    }
    
    public void setReportaPara(Integer reportaPara) {
        this.reportaPara = reportaPara;
    }
    
    public String getTituloTrabalho() {
        return tituloTrabalho;
    }
    
    public void setTituloTrabalho(String tituloTrabalho) {
        this.tituloTrabalho = tituloTrabalho;
    }
    
    public EscritorioDto getEscritorio() {
        return escritorio;
    }
    
    public void setEscritorio(EscritorioDto escritorio) {
        this.escritorio = escritorio;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.numeroEmpregado);
        hash = 47 * hash + Objects.hashCode(this.ultimoNome);
        hash = 47 * hash + Objects.hashCode(this.primeiroNome);
        hash = 47 * hash + Objects.hashCode(this.extensao);
        hash = 47 * hash + Objects.hashCode(this.email);
        hash = 47 * hash + Objects.hashCode(this.reportaPara);
        hash = 47 * hash + Objects.hashCode(this.tituloTrabalho);
        hash = 47 * hash + Objects.hashCode(this.escritorio);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpregadoDto other = (EmpregadoDto) obj;
        if (!Objects.equals(this.ultimoNome, other.ultimoNome)) {
            return false;
        }
        if (!Objects.equals(this.primeiroNome, other.primeiroNome)) {
            return false;
        }
        if (!Objects.equals(this.extensao, other.extensao)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.tituloTrabalho, other.tituloTrabalho)) {
            return false;
        }
        if (!Objects.equals(this.numeroEmpregado, other.numeroEmpregado)) {
            return false;
        }
        if (!Objects.equals(this.reportaPara, other.reportaPara)) {
            return false;
        }
        if (!Objects.equals(this.escritorio, other.escritorio)) {
            return false;
        }
        return true;
    }
    
}