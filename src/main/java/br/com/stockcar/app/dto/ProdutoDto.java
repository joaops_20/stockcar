/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class ProdutoDto implements Serializable {
    
    private String codigoProduto;
    private String nomeProduto;
    private String linhaProduto;
    private String escalaProduto;
    private String fornecedorProduto;
    private String descricaoProduto;
    private Integer quantidadeEstoque;
    private Double precoCompra;
    private Double msrp;
    private List<DetalhesPedidoDto> detalhesPedido;
    
    public String getCodigoProduto() {
        return codigoProduto;
    }
    
    public void setCodigoProduto(String codigoProduto) {
        this.codigoProduto = codigoProduto;
    }
    
    public String getNomeProduto() {
        return nomeProduto;
    }
    
    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }
    
    public String getLinhaProduto() {
        return linhaProduto;
    }
    
    public void setLinhaProduto(String linhaProduto) {
        this.linhaProduto = linhaProduto;
    }
    
    public String getEscalaProduto() {
        return escalaProduto;
    }
    
    public void setEscalaProduto(String escalaProduto) {
        this.escalaProduto = escalaProduto;
    }
    
    public String getFornecedorProduto() {
        return fornecedorProduto;
    }
    
    public void setFornecedorProduto(String fornecedorProduto) {
        this.fornecedorProduto = fornecedorProduto;
    }
    
    public String getDescricaoProduto() {
        return descricaoProduto;
    }
    
    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }
    
    public Integer getQuantidadeEstoque() {
        return quantidadeEstoque;
    }
    
    public void setQuantidadeEstoque(Integer quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }
    
    public Double getPrecoCompra() {
        return precoCompra;
    }
    
    public void setPrecoCompra(Double precoCompra) {
        this.precoCompra = precoCompra;
    }
    
    public Double getMsrp() {
        return msrp;
    }
    
    public void setMsrp(Double msrp) {
        this.msrp = msrp;
    }
    
    public List<DetalhesPedidoDto> getDetalhesPedido() {
        return detalhesPedido;
    }
    
    public void setDetalhesPedido(List<DetalhesPedidoDto> detalhesPedido) {
        this.detalhesPedido = detalhesPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.codigoProduto);
        hash = 23 * hash + Objects.hashCode(this.nomeProduto);
        hash = 23 * hash + Objects.hashCode(this.linhaProduto);
        hash = 23 * hash + Objects.hashCode(this.escalaProduto);
        hash = 23 * hash + Objects.hashCode(this.fornecedorProduto);
        hash = 23 * hash + Objects.hashCode(this.descricaoProduto);
        hash = 23 * hash + Objects.hashCode(this.quantidadeEstoque);
        hash = 23 * hash + Objects.hashCode(this.precoCompra);
        hash = 23 * hash + Objects.hashCode(this.msrp);
        hash = 23 * hash + Objects.hashCode(this.detalhesPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProdutoDto other = (ProdutoDto) obj;
        if (!Objects.equals(this.codigoProduto, other.codigoProduto)) {
            return false;
        }
        if (!Objects.equals(this.nomeProduto, other.nomeProduto)) {
            return false;
        }
        if (!Objects.equals(this.linhaProduto, other.linhaProduto)) {
            return false;
        }
        if (!Objects.equals(this.escalaProduto, other.escalaProduto)) {
            return false;
        }
        if (!Objects.equals(this.fornecedorProduto, other.fornecedorProduto)) {
            return false;
        }
        if (!Objects.equals(this.descricaoProduto, other.descricaoProduto)) {
            return false;
        }
        if (!Objects.equals(this.quantidadeEstoque, other.quantidadeEstoque)) {
            return false;
        }
        if (!Objects.equals(this.precoCompra, other.precoCompra)) {
            return false;
        }
        if (!Objects.equals(this.msrp, other.msrp)) {
            return false;
        }
        if (!Objects.equals(this.detalhesPedido, other.detalhesPedido)) {
            return false;
        }
        return true;
    }
    
}