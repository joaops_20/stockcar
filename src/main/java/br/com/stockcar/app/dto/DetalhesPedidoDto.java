/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class DetalhesPedidoDto implements Serializable {
    
    private DetalhesPedidoIdDto detalhesPedidoId;
    private Integer quantidadeEncomendada;
    private Double precoUnidade;
    private Integer numeroLinhaPedido;
    
    public DetalhesPedidoIdDto getDetalhesPedidoId() {
        return detalhesPedidoId;
    }
    
    public void setDetalhesPedidoId(DetalhesPedidoIdDto detalhesPedidoId) {
        this.detalhesPedidoId = detalhesPedidoId;
    }
    
    public Integer getQuantidadeEncomendada() {
        return quantidadeEncomendada;
    }
    
    public void setQuantidadeEncomendada(Integer quantidadeEncomendada) {
        this.quantidadeEncomendada = quantidadeEncomendada;
    }
    
    public Double getPrecoUnidade() {
        return precoUnidade;
    }
    
    public void setPrecoUnidade(Double precoUnidade) {
        this.precoUnidade = precoUnidade;
    }
    
    public Integer getNumeroLinhaPedido() {
        return numeroLinhaPedido;
    }
    
    public void setNumeroLinhaPedido(Integer numeroLinhaPedido) {
        this.numeroLinhaPedido = numeroLinhaPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.detalhesPedidoId);
        hash = 67 * hash + Objects.hashCode(this.quantidadeEncomendada);
        hash = 67 * hash + Objects.hashCode(this.precoUnidade);
        hash = 67 * hash + Objects.hashCode(this.numeroLinhaPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalhesPedidoDto other = (DetalhesPedidoDto) obj;
        if (!Objects.equals(this.detalhesPedidoId, other.detalhesPedidoId)) {
            return false;
        }
        if (!Objects.equals(this.quantidadeEncomendada, other.quantidadeEncomendada)) {
            return false;
        }
        if (!Objects.equals(this.precoUnidade, other.precoUnidade)) {
            return false;
        }
        if (!Objects.equals(this.numeroLinhaPedido, other.numeroLinhaPedido)) {
            return false;
        }
        return true;
    }
    
}