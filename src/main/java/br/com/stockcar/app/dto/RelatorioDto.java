/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class RelatorioDto implements Serializable {
    
    private String codigoEscritorio;
    private String dataInicial;
    private String dataFinal;
    
    public String getCodigoEscritorio() {
        return codigoEscritorio;
    }
    
    public void setCodigoEscritorio(String codigoEscritorio) {
        this.codigoEscritorio = codigoEscritorio;
    }
    
    public String getDataInicial() {
        return dataInicial;
    }
    
    public void setDataInicial(String dataInicial) {
        this.dataInicial = dataInicial;
    }
    
    public String getDataFinal() {
        return dataFinal;
    }
    
    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.codigoEscritorio);
        hash = 89 * hash + Objects.hashCode(this.dataInicial);
        hash = 89 * hash + Objects.hashCode(this.dataFinal);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelatorioDto other = (RelatorioDto) obj;
        if (!Objects.equals(this.codigoEscritorio, other.codigoEscritorio)) {
            return false;
        }
        if (!Objects.equals(this.dataInicial, other.dataInicial)) {
            return false;
        }
        if (!Objects.equals(this.dataFinal, other.dataFinal)) {
            return false;
        }
        return true;
    }
    
}