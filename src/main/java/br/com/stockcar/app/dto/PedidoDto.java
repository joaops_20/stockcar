/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class PedidoDto implements Serializable {
    
    private Integer numeroPedido;
    private Date dataPedido;
    private Date dataRequerida;
    private Date dataEnvio;
    private String statusPedido;
    private String comentario;
    private ClienteDto cliente;
    private List<DetalhesPedidoDto> detalhesPedido;
    
    public Integer getNumeroPedido() {
        return numeroPedido;
    }
    
    public void setNumeroPedido(Integer numeroPedido) {
        this.numeroPedido = numeroPedido;
    }
    
    public Date getDataPedido() {
        return dataPedido;
    }
    
    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }
    
    public Date getDataRequerida() {
        return dataRequerida;
    }
    
    public void setDataRequerida(Date dataRequerida) {
        this.dataRequerida = dataRequerida;
    }
    
    public Date getDataEnvio() {
        return dataEnvio;
    }
    
    public void setDataEnvio(Date dataEnvio) {
        this.dataEnvio = dataEnvio;
    }
    
    public String getStatusPedido() {
        return statusPedido;
    }
    
    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }
    
    public String getComentario() {
        return comentario;
    }
    
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    public ClienteDto getCliente() {
        return cliente;
    }
    
    public void setCliente(ClienteDto cliente) {
        this.cliente = cliente;
    }
    
    public List<DetalhesPedidoDto> getDetalhesPedido() {
        return detalhesPedido;
    }
    
    public void setDetalhesPedido(List<DetalhesPedidoDto> detalhesPedido) {
        this.detalhesPedido = detalhesPedido;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.numeroPedido);
        hash = 47 * hash + Objects.hashCode(this.dataPedido);
        hash = 47 * hash + Objects.hashCode(this.dataRequerida);
        hash = 47 * hash + Objects.hashCode(this.dataEnvio);
        hash = 47 * hash + Objects.hashCode(this.statusPedido);
        hash = 47 * hash + Objects.hashCode(this.comentario);
        hash = 47 * hash + Objects.hashCode(this.cliente);
        hash = 47 * hash + Objects.hashCode(this.detalhesPedido);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PedidoDto other = (PedidoDto) obj;
        if (!Objects.equals(this.statusPedido, other.statusPedido)) {
            return false;
        }
        if (!Objects.equals(this.comentario, other.comentario)) {
            return false;
        }
        if (!Objects.equals(this.numeroPedido, other.numeroPedido)) {
            return false;
        }
        if (!Objects.equals(this.dataPedido, other.dataPedido)) {
            return false;
        }
        if (!Objects.equals(this.dataRequerida, other.dataRequerida)) {
            return false;
        }
        if (!Objects.equals(this.dataEnvio, other.dataEnvio)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.detalhesPedido, other.detalhesPedido)) {
            return false;
        }
        return true;
    }
    
}