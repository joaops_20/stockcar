/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class DetalhesPedidoIdDto implements Serializable {
    
    private PedidoDto pedido;
    private ProdutoDto produto;
    
    public PedidoDto getPedido() {
        return pedido;
    }
    
    public void setPedido(PedidoDto pedido) {
        this.pedido = pedido;
    }
    
    public ProdutoDto getProduto() {
        return produto;
    }
    
    public void setProduto(ProdutoDto produto) {
        this.produto = produto;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.pedido);
        hash = 17 * hash + Objects.hashCode(this.produto);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalhesPedidoIdDto other = (DetalhesPedidoIdDto) obj;
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        if (!Objects.equals(this.produto, other.produto)) {
            return false;
        }
        return true;
    }
    
}