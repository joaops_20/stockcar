/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author João Paulo
 */
public class ClienteDto implements Serializable {
    
    private Integer numeroCliente;
    private String nomeCliente;
    private String ultimoNomeContato;
    private String primeiroNomeContato;
    private String telefone;
    private String endereco1;
    private String endereco2;
    private String cidade;
    private String estado;
    private String codigoPostal;
    private String pais;
    private Double limiteCredito;
    private EmpregadoDto empregado;
    
    public Integer getNumeroCliente() {
        return numeroCliente;
    }
    
    public void setNumeroCliente(Integer numeroCliente) {
        this.numeroCliente = numeroCliente;
    }
    
    public String getNomeCliente() {
        return nomeCliente;
    }
    
    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }
    
    public String getUltimoNomeContato() {
        return ultimoNomeContato;
    }
    
    public void setUltimoNomeContato(String ultimoNomeContato) {
        this.ultimoNomeContato = ultimoNomeContato;
    }
    
    public String getPrimeiroNomeContato() {
        return primeiroNomeContato;
    }
    
    public void setPrimeiroNomeContato(String primeiroNomeContato) {
        this.primeiroNomeContato = primeiroNomeContato;
    }
    
    public String getTelefone() {
        return telefone;
    }
    
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    public String getEndereco1() {
        return endereco1;
    }
    
    public void setEndereco1(String endereco1) {
        this.endereco1 = endereco1;
    }
    
    public String getEndereco2() {
        return endereco2;
    }
    
    public void setEndereco2(String endereco2) {
        this.endereco2 = endereco2;
    }
    
    public String getCidade() {
        return cidade;
    }
    
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    
    public String getEstado() {
        return estado;
    }
    
    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    public String getCodigoPostal() {
        return codigoPostal;
    }
    
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }
    
    public String getPais() {
        return pais;
    }
    
    public void setPais(String pais) {
        this.pais = pais;
    }
    
    public Double getLimiteCredito() {
        return limiteCredito;
    }
    
    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }
    
    public EmpregadoDto getEmpregado() {
        return empregado;
    }
    
    public void setEmpregado(EmpregadoDto empregado) {
        this.empregado = empregado;
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.numeroCliente);
        hash = 97 * hash + Objects.hashCode(this.nomeCliente);
        hash = 97 * hash + Objects.hashCode(this.ultimoNomeContato);
        hash = 97 * hash + Objects.hashCode(this.primeiroNomeContato);
        hash = 97 * hash + Objects.hashCode(this.telefone);
        hash = 97 * hash + Objects.hashCode(this.endereco1);
        hash = 97 * hash + Objects.hashCode(this.endereco2);
        hash = 97 * hash + Objects.hashCode(this.cidade);
        hash = 97 * hash + Objects.hashCode(this.estado);
        hash = 97 * hash + Objects.hashCode(this.codigoPostal);
        hash = 97 * hash + Objects.hashCode(this.pais);
        hash = 97 * hash + Objects.hashCode(this.limiteCredito);
        hash = 97 * hash + Objects.hashCode(this.empregado);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteDto other = (ClienteDto) obj;
        if (!Objects.equals(this.nomeCliente, other.nomeCliente)) {
            return false;
        }
        if (!Objects.equals(this.ultimoNomeContato, other.ultimoNomeContato)) {
            return false;
        }
        if (!Objects.equals(this.primeiroNomeContato, other.primeiroNomeContato)) {
            return false;
        }
        if (!Objects.equals(this.telefone, other.telefone)) {
            return false;
        }
        if (!Objects.equals(this.endereco1, other.endereco1)) {
            return false;
        }
        if (!Objects.equals(this.endereco2, other.endereco2)) {
            return false;
        }
        if (!Objects.equals(this.cidade, other.cidade)) {
            return false;
        }
        if (!Objects.equals(this.estado, other.estado)) {
            return false;
        }
        if (!Objects.equals(this.codigoPostal, other.codigoPostal)) {
            return false;
        }
        if (!Objects.equals(this.pais, other.pais)) {
            return false;
        }
        if (!Objects.equals(this.numeroCliente, other.numeroCliente)) {
            return false;
        }
        if (!Objects.equals(this.limiteCredito, other.limiteCredito)) {
            return false;
        }
        if (!Objects.equals(this.empregado, other.empregado)) {
            return false;
        }
        return true;
    }
    
}