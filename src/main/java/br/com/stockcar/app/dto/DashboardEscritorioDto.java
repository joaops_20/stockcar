/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.dto;

import java.io.Serializable;

/**
 *
 * @author João
 */
public class DashboardEscritorioDto implements Serializable {
    
    private Integer ano;
    private Double janeiro;
    private Double fevereiro;
    private Double marco;
    private Double abril;
    private Double maio;
    private Double junho;
    private Double julho;
    private Double agosto;
    private Double setembro;
    private Double outubro;
    private Double novembro;
    private Double dezembro;
    
    public Integer getAno() {
        return ano;
    }
    
    public void setAno(Integer ano) {
        this.ano = ano;
    }
    
    public Double getJaneiro() {
        return janeiro;
    }
    
    public void setJaneiro(Double janeiro) {
        this.janeiro = janeiro;
    }
    
    public Double getFevereiro() {
        return fevereiro;
    }
    
    public void setFevereiro(Double fevereiro) {
        this.fevereiro = fevereiro;
    }
    
    public Double getMarco() {
        return marco;
    }
    
    public void setMarco(Double marco) {
        this.marco = marco;
    }
    
    public Double getAbril() {
        return abril;
    }
    
    public void setAbril(Double abril) {
        this.abril = abril;
    }
    
    public Double getMaio() {
        return maio;
    }
    
    public void setMaio(Double maio) {
        this.maio = maio;
    }
    
    public Double getJunho() {
        return junho;
    }
    
    public void setJunho(Double junho) {
        this.junho = junho;
    }
    
    public Double getJulho() {
        return julho;
    }
    
    public void setJulho(Double julho) {
        this.julho = julho;
    }
    
    public Double getAgosto() {
        return agosto;
    }
    
    public void setAgosto(Double agosto) {
        this.agosto = agosto;
    }
    
    public Double getSetembro() {
        return setembro;
    }
    
    public void setSetembro(Double setembro) {
        this.setembro = setembro;
    }
    
    public Double getOutubro() {
        return outubro;
    }
    
    public void setOutubro(Double outubro) {
        this.outubro = outubro;
    }
    
    public Double getNovembro() {
        return novembro;
    }
    
    public void setNovembro(Double novembro) {
        this.novembro = novembro;
    }
    
    public Double getDezembro() {
        return dezembro;
    }
    
    public void setDezembro(Double dezembro) {
        this.dezembro = dezembro;
    }
    
}