/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.mapper;

import br.com.stockcar.app.dto.ProdutoDto;
import br.com.stockcar.app.model.Produto;
import org.dozer.loader.api.BeanMappingBuilder;

/**
 *
 * @author João Paulo
 */
public class ProdutoMapper extends BeanMappingBuilder {
    
    @Override
    protected void configure() {
        this.mapping(Produto.class, ProdutoDto.class);
    }
    
}