/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.mapper;

import br.com.stockcar.app.dto.SystemUserPermissionIdDto;
import br.com.stockcar.app.model.SystemUserPermissionId;
import org.dozer.loader.api.BeanMappingBuilder;

/**
 *
 * @author João Paulo
 */
public class SystemUserPermissionIdMapper extends BeanMappingBuilder {
    
    @Override
    protected void configure() {
        this.mapping(SystemUserPermissionId.class, SystemUserPermissionIdDto.class);
    }
    
}