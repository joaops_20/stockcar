/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.mapper;

import br.com.stockcar.app.dto.ClienteDto;
import br.com.stockcar.app.model.Cliente;
import org.dozer.loader.api.BeanMappingBuilder;

/**
 *
 * @author João Paulo
 */
public class ClienteMapper extends BeanMappingBuilder {
    
    @Override
    protected void configure() {
        this.mapping(Cliente.class, ClienteDto.class);
    }
    
}