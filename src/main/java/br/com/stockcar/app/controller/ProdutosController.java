/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.ProdutoDto;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import br.com.stockcar.app.service.ProdutoService;

/**
 *
 * @author João
 */
@Controller
public class ProdutosController {
    
    @Autowired
    private ProdutoService produtoService;
    
    @RequestMapping(value = "/produtos", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("produtos/index");
        PageWrapper<ProdutoDto> page = new PageWrapper<>(produtoService.searchAll(p), "/produtos");
        mav.addObject("page", page);
        return mav;
    }
    
}