/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.SystemUserDto;
import br.com.stockcar.app.dto.SystemUserFormDto;
import br.com.stockcar.app.service.SystemUserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author João Paulo
 */
@Controller
@RequestMapping(value = "system/user")
public class SystemUserController {
    
    @Autowired
    private SystemUserService systemUserService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("/system/user/index");
        Page<SystemUserDto> page = systemUserService.searchAllUsers(p);
        mav.addObject("page", page);
        return mav;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("/system/user/add");
        mav.addObject("sysuser", systemUserService.newSystemUser());
        return mav;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(@Valid SystemUserFormDto sysuser, BindingResult result, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav;
        if (result.hasErrors()) {
            mav = new ModelAndView("/system/user/add");
            mav.addObject("sysuser", sysuser);
        } else {
            mav = new ModelAndView("redirect:/system/user/");
            systemUserService.save(sysuser);
        }
        return mav;
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("redirect:/system/user/");
        systemUserService.delete(systemUserService.findOne(id));
        return mav;
    }
    
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public ModelAndView edit(@PathVariable Long id, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("/system/user/edit");
        mav.addObject("sysuser", systemUserService.findOneSystemUserFormDto(id));
        return mav;
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public ModelAndView update(@Valid SystemUserFormDto sysuser, BindingResult result, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav;
        if (result.hasErrors()) {
            mav = new ModelAndView("/system/user/edit");
            mav.addObject("sysuser", sysuser);
        } else {
            mav = new ModelAndView("redirect:/system/user/");
            systemUserService.save(sysuser);
        }
        return mav;
    }
    
}