/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.DashboardEscritorioDto;
import br.com.stockcar.app.dto.EscritorioDto;
import br.com.stockcar.app.dto.RelatorioDto;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import br.com.stockcar.app.service.EscritorioService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author João
 */
@Controller
public class EscritorioController {
    
    @Autowired
    private EscritorioService escritorioService;
    
    @RequestMapping(value = "/escritorios", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("escritorios/index");
        PageWrapper<EscritorioDto> page = new PageWrapper<>(escritorioService.searchAll(p), "/escritorios");
        mav.addObject("page", page);
        DashboardEscritorioDto dashboard = escritorioService.findDashboard(Calendar.getInstance().get(Calendar.YEAR));
        mav.addObject("dashboad", dashboard);
        mav.addObject("janeiro", dashboard.getJaneiro());
        mav.addObject("fevereiro", dashboard.getFevereiro());
        mav.addObject("marco", dashboard.getMarco());
        mav.addObject("abril", dashboard.getAbril());
        mav.addObject("maio", dashboard.getMaio());
        mav.addObject("junho", dashboard.getJunho());
        mav.addObject("julho", dashboard.getJulho());
        mav.addObject("agosto", dashboard.getAgosto());
        mav.addObject("setembro", dashboard.getSetembro());
        mav.addObject("outubro", dashboard.getOutubro());
        mav.addObject("novembro", dashboard.getNovembro());
        mav.addObject("dezembro", dashboard.getDezembro());
        mav.addObject("relatorioDto", new RelatorioDto());
        return mav;
    }
    
    @RequestMapping(value = "/escritorios", method = RequestMethod.POST)
    public ModelAndView indexPost(DashboardEscritorioDto dashboard, HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("escritorios/index");
        PageWrapper<EscritorioDto> page = new PageWrapper<>(escritorioService.searchAll(p), "/escritorios");
        mav.addObject("page", page);
        dashboard = escritorioService.findDashboard(dashboard.getAno());
        mav.addObject("dashboad", dashboard);
        mav.addObject("janeiro", dashboard.getJaneiro());
        mav.addObject("fevereiro", dashboard.getFevereiro());
        mav.addObject("marco", dashboard.getMarco());
        mav.addObject("abril", dashboard.getAbril());
        mav.addObject("maio", dashboard.getMaio());
        mav.addObject("junho", dashboard.getJunho());
        mav.addObject("julho", dashboard.getJulho());
        mav.addObject("agosto", dashboard.getAgosto());
        mav.addObject("setembro", dashboard.getSetembro());
        mav.addObject("outubro", dashboard.getOutubro());
        mav.addObject("novembro", dashboard.getNovembro());
        mav.addObject("dezembro", dashboard.getDezembro());
        mav.addObject("relatorioDto", new RelatorioDto());
        return mav;
    }
    
    @RequestMapping(value = "/escritorios/relatorio-vendas", method = RequestMethod.POST)
    public ModelAndView gerarRelatorioDetalhado(RelatorioDto relatorioDto, HttpServletRequest request, HttpServletResponse response, Pageable p) {
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("format", "pdf");
        parametros.put("codigo_escritorio", relatorioDto.getCodigoEscritorio());
        Date dataInicial, dataFinal;
        try {
            dataInicial = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(relatorioDto.getDataInicial()+" 00:00:00");
            dataFinal = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(relatorioDto.getDataFinal()+" 23:59:59");
        } catch (Exception e) {
            Calendar c = Calendar.getInstance();
            c.set(2000, 1, 1, 0, 0, 0);
            dataInicial = c.getTime();
            dataFinal = Calendar.getInstance().getTime();
        }
        parametros.put("data_inicial", dataInicial);
        parametros.put("data_final", dataFinal);
        return new ModelAndView("relatorio_vendas_escritorio", parametros);
    }
    
}