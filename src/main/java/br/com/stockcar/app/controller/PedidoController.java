/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.PedidoDto;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import br.com.stockcar.app.service.PedidoService;

/**
 *
 * @author João
 */
@Controller
public class PedidoController {
    
    @Autowired
    private PedidoService pedidoService;
    
    @RequestMapping(value = "/pedidos", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("pedidos/index");
        PageWrapper<PedidoDto> page = new PageWrapper<>(pedidoService.searchAll(p), "/pedidos");
        mav.addObject("page", page);
        return mav;
    }
    
}