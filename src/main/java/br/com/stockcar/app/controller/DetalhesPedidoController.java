/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.DetalhesPedidoDto;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import br.com.stockcar.app.service.DetalhesPedidoService;

/**
 *
 * @author João Paulo
 */
@Controller
public class DetalhesPedidoController {
    
    @Autowired
    private DetalhesPedidoService detalhesPedidoService;
    
    @RequestMapping(value = "/detalhespedidos", method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("detalhespedidos/index");
        PageWrapper<DetalhesPedidoDto> page = new PageWrapper<>(detalhesPedidoService.searchAll(p), "/detalhespedidos");
        mav.addObject("page", page);
        return mav;
    }
    
}