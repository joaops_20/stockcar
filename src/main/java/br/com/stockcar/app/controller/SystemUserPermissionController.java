/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.stockcar.app.controller;

import br.com.stockcar.app.dto.SystemUserPermissionFormDto;
import br.com.stockcar.app.service.SystemModuleService;
import br.com.stockcar.app.service.SystemUserPermissionService;
import br.com.stockcar.app.service.SystemUserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author João Paulo
 */
@Controller
@RequestMapping("system/permission")
public class SystemUserPermissionController {
    
    @Autowired
    private SystemUserPermissionService systemUserPermissionService;
    
    @Autowired
    private SystemUserService systemUserService;
    
    @Autowired
    private SystemModuleService systemModuleService;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("/system/permission/index");
        mav.addObject("page", systemUserPermissionService.searchAllUsersPermissions(p));
        return mav;
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public ModelAndView add(HttpServletRequest request, HttpServletResponse response, Pageable p) {
        ModelAndView mav = new ModelAndView("/system/permission/add");
        mav.addObject("permission", systemUserPermissionService.newSystemUserPermission());
        mav.addObject("modules", systemModuleService.searchAllModules(p));//p ? mais que 10?
        mav.addObject("users", systemUserService.searchAllUsers(p));
        return mav;
    }
    
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView save(SystemUserPermissionFormDto permissionDto, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("redirect:/system/permission");
        systemUserPermissionService.save(permissionDto);
        return mav;
    }
    
    @RequestMapping(value = "/delete/{idUser}/{idModule}", method = RequestMethod.GET)
    public ModelAndView delete(@PathVariable Long idUser, @PathVariable Long idModule, HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("redirect:/system/permission/");
        //SystemUserPermissionIdDto id = new SystemUserPermissionIdDto();
        //id.setSystemUser(systemUserService.findOne(idUser));
        //id.setSystemModule(systemModuleService.findOne(idModule));
        //systemUserPermissionService.delete(id);
        
        //systemUserPermissionService.delete(systemUserPermissionService.findOne(idUser, idModule));
        systemUserPermissionService.delete(idUser, idModule);
        return mav;
    }
    
}